# tiny-apps
####  

[TOC]



#### :round_pushpin: 保存一些**常用的**&**小而美的**开发相关的软件与工具

- Match-Tracer.zip---------------------正则表达式匹配工具
- Textify.zip--------------------------复制Windows窗口文字
- easyshell.zip------------------------Eclipse小插件，方便browse工程目录
- fast-stone.zip-----------------------强大的截图和桌面复制工具
- hfs_2.3.282.1395216160.exe-----------hfs网络文件服务器，便于文件传输。（好处是：不用像飞秋那样，两边都装）
- jd-gui.zip---------------------------反编译工具
- luyten------------------------------反编译工具，比jd-gui更强大好用（https://github.com/deathmarine/Luyten/releases）
- 腾讯截图2012.exe----------------------小巧的截图工具
- sublime text-------------------------小巧而强大的文本编辑器，适合用来编写html，有提示。（Notepad++、EditPlus也都还可以）
- vieas -------------------------------轻量、快速的 Windows 看图工具 <http://www.vieas.com/en/software/vieas.html>
- QQWubiZiGen.exe ---------------------五笔字根
- FoxonicPro----------------------------pdf切割、合并、转换、编辑工具
- SwitchHosts—--------------------------hosts管理利器
- Hotkey Exploer------------------------检查热键冲突
- Everything ---------------------------查找文件
- E-draw -------------------------------亿图图示专家（画图软件，和visio类似）<http://jingyan.baidu.com/article/22fe7ced25b6023003617f6d.html?st=2&os=0&bd_page_type=1&net_type=1>
- PatchNavicat -------------------------Navicat 产品破解器
- ZooInspector -------------------------ZooKeeper的目录查看器
- lantern ------------------------------翻墙工具，轻松就可以访问google、facebook等被墙的网站。<https://github.com/getlantern/forum/issues/833>
- mvn_download -------------------------自动下载依赖 jar 到本地
- LICEcap ------------------------------[屏幕录制工具（可导出gif）](http://www.cockos.com/licecap/)
- ReliefJetQuicksOutlook ---------------[outlook关闭不退出](http://www.reliefjet.com/Quicks/Features/QuickTweaks)
- Getman -------------------------------在线postman工具<https://getman.cn/>（需要将chrome设置为允许跨域）
- Typora---------------------------------Markdown写作工具
- Notable-------------------------------Markdown 笔记管理（目录、检索....） https://github.com/notable/notable
- MobaXterm--------------------------Windows全能终端神器
- cmder---------------------------------windows终端工具cmd的替代者
- BandZIP------------------------------压缩工具
- PicGo----------------------------------贴图工具，支持多种图床，功能强大
- QuickLook---------------------------快速预览文件（按空格）
- snipaste------------------------------截屏工具
- ditto-----------------------------------复制管理器
- DataGrip-----------------------------一个客户端搞定主流数据库
- Pointofix----------------------------显示器屏幕彩色画笔
- classpy-------------------------------字节码研究工具

------


#### :triangular_flag_on_post: 列举一些好用的chrome浏览器插件

- EditThisCookie 修改cookie用  

- postman 模拟发送请求  

- restlet client  发送 http 请求

- JSON Viewer 对ajax请求返回的json数据进行json格式化  

- Tampermonkey （这个牛逼）是一款免费的浏览器扩展和最为流行的用户脚本管理器，它适用于 Chrome, Microsoft Edge, Safari, Opera Next, 和 Firefox。 
  脚本插件网：  
  http://www.cnplugins.com/top/  
  http://www.cnplugins.com/zhuanti/  

- easy copy 破解网站禁止复制的插件  

- OneTab  管理你打开的网页，优化 chrome 吃内存的问题

- Video Downloader professional  自动识别下载资源

- IDM(Intenet Download Manager)：下载视频

- infinity  自定义标签页&书签管理

- Adblock Plus：屏蔽广告

- octotree:github文件树

- GitCodeTree:gitos文件树

- oscnews:新标签页插件，查看开源中国软件更新资讯https://gitee.com/jaywcjlove/oscnews

- 掘金：新标签页插件

- vimium:摆脱鼠标神器<https://blog.csdn.net/qq_36084640/article/details/79320883>

- Extension Manager：扩展管理器

- 惠惠购物助手：剁手党看历史价格

- 微博图床：一键将页面中的图片上传到微博

  ------


#### :peach: 列举一些好用的IDEA插件

- IDEA破解码、补丁、keygen <http://idea.lanyus.com/>、<https://blog.csdn.net/qq_24504453/article/details/77407329>、<https://blog.csdn.net/jdjdndhj/article/details/79464052>
- jclasslib 字节码查看工具  https://blog.csdn.net/qq_34039315/article/details/78561493
- mybatis 
- Rainbow Brackets-----彩虹括号
- Maven Helper-----查看maven依赖和冲突，一目了然
- vim

------

#### :beginner: 其他

磁力链 & torrent 下载工具： Bitcomet,BitSpirit  
dump分析工具： https://thread.console.perfma.com/  
Another Redis Desktop Manager： redis 客户端